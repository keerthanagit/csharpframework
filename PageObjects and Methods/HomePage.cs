﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Pages
{
    public class HomePage 
    {
    #region Elements
        private IWebElement IndustryLabel = FrameWork.driver.FindElement(By.XPath("//*[@id='menu']/li/div/a[contains(@href,'industries')]/span[text()='Industries']"));
        private IWebElement BuildAutomationLabel = FrameWork.driver.FindElement(By.XPath("//*[@id='menu']/li[contains(@class,'last-child')]/div/div/ul/li/a[contains(@href,'building-automation')]"));
        private IWebElement ProductsLable = FrameWork.driver.FindElement(By.CssSelector(".newnav a[href^='/product']"));
        private IWebElement JobCenter = FrameWork.driver.FindElement(By.CssSelector("li.newnav.right:nth-child(4)"));
        private IWebElement salarySurveyResult2018 = FrameWork.driver.FindElement(By.XPath("//div[@id='header']//li[4]//div[1]//div[1]//li[3]"));
        #endregion

        #region Methods
        public void HoverAndClickingBuildAutomationLabel()
        {
            Actions actions = new Actions(FrameWork.driver);
            actions.MoveToElement(IndustryLabel).Build().Perform();
            BuildAutomationLabel.Click();
        }
     
        public void ClickOnProductsLable()
        {
            ProductsLable.Click();
        }
        public void GetTitle(IWebElement webElement)
        {
            webElement.GetAttribute("textContent");
        }
        public void HoverAndClickingSalarySurveyListPage()
        {
            Actions actions = new Actions(FrameWork.driver);
            actions.MoveToElement(JobCenter).Build().Perform();
            salarySurveyResult2018.Click();
        }
        #endregion
    }
}
