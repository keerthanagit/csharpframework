﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Pages
{
    public class ProductsPage
    {
        public string finalText;
        #region Elements

        public IWebElement ProductSearchTitle = FrameWork.driver.FindElement(By.XPath("//div[@class='block']/h1"));

        #endregion

        #region Methods

        public void GetTitle(IWebElement webElement)
        {
           finalText = webElement.GetAttribute("textContent");
        }

        public void VerifyTitle(string finalText,string expectedTitle)
        {
            Assert.AreEqual(finalText,expectedTitle);
        }
        #endregion
    }
}
